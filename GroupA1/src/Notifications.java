import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Notifications extends JFrame implements ActionListener{

	
	// DB Connectivity Attributes
		private Connection con = null;
		private Statement stmt = null;
		private ResultSet rs = null;

	
		JLabel titleLabel = new JLabel ("College Library Add/Update Book");
		JButton submitButton = new JButton("Submit");
		JLabel studentnoLabel = new JLabel ("Student No");
		JTextField username = new JTextField(9);
		JLabel studentnameLabel = new JLabel ("Student Name");
		JTextField studentname = new JTextField(30);
		JLabel studentemailLabel = new JLabel ("Student Email");
		JTextField studentemail = new JTextField(30);
		JLabel passLabel = new JLabel ("Password");
		JPasswordField password = new JPasswordField(15);
		JLabel messageLabel = new JLabel ("Your Student Number will act as your username");
		JPanel panel = new JPanel();

		Notifications(){
		super("College Library");
		setSize(500,250);
		setLocation(400,280);
		panel.setLayout (null); 

		titleLabel.setBounds(120,0,200,20);
		messageLabel.setBounds(70,120,300,30);
		submitButton.setBounds(170,150,100,30);
		studentnoLabel.setBounds(70,30,100,20);
		username.setBounds(170,30,100,20);
		studentnameLabel.setBounds(70,50,100,20);
		studentname.setBounds(170,50,100,20);
		studentemailLabel.setBounds(70,70,100,20);
		studentemail.setBounds(170,70,100,20);
		passLabel.setBounds(70,90,100,20);
		password.setBounds(170,90,100,20);
		
			
		panel.add(titleLabel);
		panel.add(messageLabel);
		panel.add(submitButton);
		panel.add(studentnoLabel);
		panel.add(username);
		panel.add(studentnameLabel);
		panel.add(studentname);
		panel.add(studentemailLabel);
		panel.add(studentemail);
		panel.add(passLabel);
		panel.add(password);
		
		submitButton.addActionListener(this);

		getContentPane().add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	//event handling 
		public void actionPerformed(ActionEvent e)
		{
			Object target=e.getSource();
			if (target == submitButton)
			{
				try
				{
					String p = String.valueOf(password.getPassword());
					String Temp ="INSERT INTO students VALUES("+
							null +",'"+username.getText()+"','"+studentname.getText()+"',"+studentemail.getText()+",'"+p+");";

					stmt.executeUpdate(Temp);
					
					String updateTemp ="INSERT INTO student_login VALUES("+null +",'"+username.getText()+"','"+p+");";

					stmt.executeUpdate(updateTemp);

				}
				catch (SQLException sqle)
				{
					System.err.println("Error with  insert:\n"+sqle.toString());
				}
				JOptionPane.showMessageDialog (null, "Student Reg Successful", "Student eg", JOptionPane.INFORMATION_MESSAGE);
					JFrame LoginGui = new JFrame();
					new LoginGui();
					dispose();}			
			}
	
	public static void main(String[] args) {
		JFrame Menu = new JFrame();
		}

	}
