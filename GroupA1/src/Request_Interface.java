import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.print.Book;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Request_Interface {

	public JFrame frame;
	private JTextField ISBN;
	private JTextField TITLE;
	private JTextField AUTHOR;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Request_Interface window = new Request_Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public void initiate_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/Agile_Sprint1";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			System.out.println("Connected");
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n"
					+ e.getMessage());
		}
	}

	/**
	 * Create the application.
	 */
	public Request_Interface() {
		initialize();
		initiate_conn();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 281, 315);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		JLabel lblNewLabel_1 = new JLabel("ISBN : ");
		lblNewLabel_1.setBounds(22, 92, 54, 22);
		lblNewLabel_1.setFont(new Font("Georgia", Font.PLAIN, 16));
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Title : ");
		lblNewLabel_2.setBounds(22, 124, 66, 22);
		lblNewLabel_2.setFont(new Font("Georgia", Font.PLAIN, 16));
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Author : ");
		lblNewLabel_3.setFont(new Font("Georgia", Font.PLAIN, 16));
		lblNewLabel_3.setBounds(22, 158, 66, 22);
		frame.getContentPane().add(lblNewLabel_3);
		
		ISBN = new JTextField();
		ISBN.setBounds(143, 94, 93, 21);
		frame.getContentPane().add(ISBN);
		ISBN.setColumns(10);
		
		TITLE = new JTextField();
		TITLE.setBounds(143, 126, 93, 21);
		frame.getContentPane().add(TITLE);
		TITLE.setColumns(10);
		
		AUTHOR = new JTextField();
		AUTHOR.setBounds(143, 160, 93, 21);
		frame.getContentPane().add(AUTHOR);
		AUTHOR.setColumns(10);
		
		ButtonGroup bg=new ButtonGroup();
		JRadioButton book=new JRadioButton("BOOK");
		bg.add(book);
		frame.getContentPane().add(book);
		book.setBounds(22, 202, 93, 22);
		JRadioButton journal= new JRadioButton("JOURNAL");
		journal.setBounds(117, 202, 100, 23);
		bg.add(journal);
		frame.getContentPane().add(journal);
		
		
		JButton btnNewButton = new JButton("Sent Request");
		btnNewButton.setFont(new Font("Georgia", Font.PLAIN, 16));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					String type=null;
//					JRadioButton book=(JRadioButton)arg0.getSource();
//					JRadioButton journal=(JRadioButton)arg0.getSource();
					if(book.isSelected()){
						type="Book";
					}else if(journal.isSelected()){
						type="Journal";
					}
					
					String Temp ="INSERT INTO requests VALUES("+
							null +","+ISBN.getText()+",'"+TITLE.getText()+"','"+AUTHOR.getText()+"','"+type+"');";

					stmt.executeUpdate(Temp);
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(53, 243, 153, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblRequest = new JLabel("Request");
		lblRequest.setFont(new Font("Script MT Bold", Font.BOLD | Font.ITALIC, 20));
		lblRequest.setBounds(84, 22, 93, 49);
		frame.getContentPane().add(lblRequest);
		
		
		
		
		
		
	}
}