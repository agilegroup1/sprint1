package BookJournalsTest;


public class Journals  {

	private int journalID;
	private String journalTitle;
	private int ISBN_J;
	private String Author_J;
	private int quantity;
	private double JournalPrice;
	
	//JournalsPublisher T;
	//T = new JournalsPublisher();

	

	public Journals(String journalTitle, int iSBN_J, String author_J,int quantity, double journalPrice) {
		
		this.journalTitle = journalTitle;
		this.ISBN_J = iSBN_J;
		this.Author_J = author_J;
		this.quantity = quantity;
		this.JournalPrice = journalPrice;
	}

	

	public final int getJournalID() {
		return journalID;
	}

	public final void setJournalID(int journal_ID) {
		this.journalID = journal_ID;
	}



	public final String getjournalTitle() {
		return journalTitle;
	}

	public final void setjournalTitle(String journalTitle) {
		this.journalTitle = journalTitle;
	}

	public final int getISBN_J() {
		return ISBN_J;
	}

	public final void setISBN_J(int iSBN_J) {
		ISBN_J = iSBN_J;
	}
	public final String getAuthor_J() {
		return Author_J;
	}

	public final void setAuthor_J(String author_J) {
		Author_J = author_J;
	}
	
	public final int getQuantity() {
		return quantity;
	}

	public final void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public final double getjournalPrice() {
		return JournalPrice;
	}

	public final void setjournalPrice(double journalPrice) {
		this.JournalPrice = journalPrice;
	}
	
	
	

	


}
