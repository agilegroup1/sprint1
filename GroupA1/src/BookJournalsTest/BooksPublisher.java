package BookJournalsTest;

public class BooksPublisher {
private String publisherName;
private int publisherNumber;
private  String publisherAddress;
private  String  publisherEmail;
	
	
	public BooksPublisher(String p_name, int p_no, String p_addr, String p_em) {
	
		
		this.publisherName = p_name;
		this.publisherNumber = p_no;
		this.publisherAddress = p_addr;
		this.publisherEmail = p_em;
	}


	public final String getPublisherName() {
		return publisherName;
	}


	public final void setPublisherName(String p_name) {
		this.publisherName = p_name;
	}


	public final int getPublisherNumber() {
		return publisherNumber;
	}


	public final void setPublisherNumber(int p_no) {
		this.publisherNumber = p_no;
	}


	public final String getPublisherAddress() {
		return publisherAddress;
	}


	public final void setPublisherAddress(String p_addr) {
		this.publisherAddress = p_addr;
	}


	public final String getPublisherEmail() {
		return publisherEmail;
	}


	public final void setPublisherEmail(String p_em) {
		this.publisherEmail = p_em;
	}
}