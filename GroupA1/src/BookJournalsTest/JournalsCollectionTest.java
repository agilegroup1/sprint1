package BookJournalsTest;


import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;



public class JournalsCollectionTest  extends TestCase{
		
		// Test Number: 1
		// Test Objective: To test count Number of the journals
		// Input(s): Empty Journal list;
		// Expected Output(s) = "Journal list is empty"

		
		@Test
		public void testCreateEmptyJournalsCollection() 
		{
		
			
		
			JournalsCollection journalsCol = new JournalsCollection();
			Assert.assertEquals(" Journal list is empty ",0, journalsCol.getJournalsCount());
			
			
		

		}

		
		// Test Number: 2
		// Test Objective: To test Add one Journal to the list
		// Input(s): journalTitle , ISBN , Author , , price ;
		// Expected Output(s) = "Journal list >> journal added"
	
		@Test
		public void testAddSingleJournalToJournalsCollection() {
			
			JournalsCollection journalsCol = new JournalsCollection();
			Journals journal = new Journals("Learn Agile",1234567,"Ahmed" ,1, 30.0);
			journalsCol.addJournal(journal);
			Assert.assertEquals(" Single Journal added ",1, journalsCol.getJournalsCount());
			
		}
		
		
		// Test Number: 3
		// Test Objective: To test Add two journals 
		// Input(s):  Add 2 journals info to DB
		// Expected Output(s) = "2 Journals added"
		@Test
		public void testAddTwoJournalsToJournalsCollection() {
			JournalsCollection journalsCol = new JournalsCollection();
			Journals journal = new Journals("Learn Agile",1234567,"Ahmed" ,1, 30.0);
			Journals journal2 = new Journals("Athlone",1234567,"J M" ,2, 60.0);
			journalsCol.addJournal(journal);journalsCol.addJournal(journal2);
			Assert.assertEquals(" Two journals added ",2, journalsCol.getJournalsCount());
		}
		
	
		// Test Number: 4
		// Test Objective: To test an empty Publisher list
		// Input(s): Empty Publisher list;
		// Expected Output(s) = "Publisher list is empty"
		@Test
		public void testCreateEmptyJournalsPublisherCollection() {
			JournalsCollection2 journalsCol2 = new JournalsCollection2();
			Assert.assertEquals(" Publisher list is empty ",0, journalsCol2.getJournalsPublisherCount());
		}
		//Test for add single Publisher
		// Test Number: 5
		// Test Objective: To test an empty Publisher list
		// Input(s): Empty Publisher list;
		// Expected Output(s) = "Publisher list is empty"
		@Test
		public void testAddSingleJournalsPublisherCollection() {
			JournalsCollection2 journalsCol2 = new JournalsCollection2();
			JournalsPublisher JournalsPub = new JournalsPublisher("AIT",87777777,"Dublin Rd","info@ait.ie");
			journalsCol2.addJournalPublisher(JournalsPub);
			Assert.assertEquals(" Single Journal Publisher added ",1, journalsCol2.getJournalsPublisherCount());
		}
		
		// Test Number: 6
		// Test Objective: To Test for add Two Publisher
		// Input(s): Empty Publisher list;
		// Expected Output(s) = "add Two Publishers info"
		 
				@Test
		public void testAddTwoJournalsPublisherCollection() {
				JournalsCollection2 journalsCol2 = new JournalsCollection2();
				JournalsPublisher JournalsPub = new JournalsPublisher("AIT",87777777,"Dublin Rd","info@ait.ie");
				journalsCol2.addJournalPublisher(JournalsPub);
				JournalsPublisher JournalsPub2 = new JournalsPublisher("ATHLONE m",8771234,"Dublin Rd","info@athlone.ie");
				journalsCol2.addJournalPublisher(JournalsPub2);
				Assert.assertEquals(" Two Journals Publisher added ",2, journalsCol2.getJournalsPublisherCount());
				}
		
}

