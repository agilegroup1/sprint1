package BookJournalsTest;


import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;



public class BooksCollectionTest  extends TestCase{
		
		// Test Number: 1
		// Test Objective: To test count Number of the books
		// Input(s): Empty book list;
		// Expected Output(s) = "Book list is empty"

		
		@Test
		public void testCreateEmptyBooksCollection() 
		{
		
			
		
			BooksCollection booksCol = new BooksCollection();
			Assert.assertEquals(" Book list is empty ",0, booksCol.getBooksCount());
			
			
		

		}

		
		// Test Number: 2
		// Test Objective: To test Add one book to the list
		// Input(s): bookTitle , ISBN , Author , , price ;
		// Expected Output(s) = "Book list book added"
	
		@Test
		public void testAddSingleBookToBooksCollection() {
			
			BooksCollection booksCol = new BooksCollection();
			Books book = new Books("Learn Agile",1234567,"Ahmed" ,1, 30.0);
			booksCol.addBook(book);
			Assert.assertEquals(" Single Book added ",1, booksCol.getBooksCount());
			
		}
		
		
		// Test Number: 3
		// Test Objective: To test Add two books 
		// Input(s):  Add 2 books info to DB
		// Expected Output(s) = "2 Journals added"
		@Test
		public void testAddTwoBooksToBooksCollection() {
			BooksCollection booksCol = new BooksCollection();
			Books book = new Books("Learn Agile",1234567,"Ahmed" ,1, 30.0);
			Books book2 = new Books("Athlone",1234567,"J M" ,2, 60.0);
			booksCol.addBook(book);booksCol.addBook(book2);
			Assert.assertEquals(" Two books added ",2, booksCol.getBooksCount());
		}
		
	
		// Test Number: 4
		// Test Objective: To test an empty Publisher list
		// Input(s): Empty Publisher list;
		// Expected Output(s) = "Publisher list is empty"
		@Test
		public void testCreateEmptyBooksPublisherCollection() {
			BooksCollection2 booksCol2 = new BooksCollection2();
			Assert.assertEquals(" Publisher list is empty ",0, booksCol2.getBooksPublisherCount());
		}
		//Test for add single Publisher
		// Test Number: 5
		// Test Objective: To test an empty Publisher list
		// Input(s): Empty Publisher list;
		// Expected Output(s) = "Publisher list is empty"
		@Test
		public void testAddSingleBooksPublisherCollection() {
			BooksCollection2 booksCol2 = new BooksCollection2();
			BooksPublisher BooksPub = new BooksPublisher("AIT",87777777,"Dublin Rd","info@ait.ie");
			booksCol2.addBookPublisher(BooksPub);
			Assert.assertEquals(" Single Book Publisher added ",1, booksCol2.getBooksPublisherCount());
		}
		
		// Test Number: 6
		// Test Objective: To Test for add Two Publisher
		// Input(s): Empty Publisher list;
		// Expected Output(s) = "add Two Publishers info"
		 
				@Test
		public void testAddTwoBooksPublisherCollection() {
				BooksCollection2 booksCol2 = new BooksCollection2();
				BooksPublisher BooksPub = new BooksPublisher("AIT",87777777,"Dublin Rd","info@ait.ie");
				booksCol2.addBookPublisher(BooksPub);
				BooksPublisher BooksPub2 = new BooksPublisher("ATHLONE m",8771234,"Dublin Rd","info@athlone.ie");
				booksCol2.addBookPublisher(BooksPub2);
				Assert.assertEquals(" Two Journals Publisher added ",2, booksCol2.getBooksPublisherCount());
				}
		
}

