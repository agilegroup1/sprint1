package BookJournalsTest;


import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


//this is for Publisher list


public class JournalsCollection2 {
	
	 private List<JournalsPublisher> journals_P_List = new ArrayList<>();
	
	public int getJournalsPublisherCount() {
		return journals_P_List.size();
	}
	public void addJournalPublisher(JournalsPublisher JournalsPub) {
		
		journals_P_List.add(JournalsPub);
	}
 
}
