package BookJournalsTest;


import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


//this is for Publisher list


public class BooksCollection2 {
	
	 private List<BooksPublisher> books_P_List = new ArrayList<>();
	
	public int getBooksPublisherCount() {
		return books_P_List.size();
	}
	public void addBookPublisher(BooksPublisher BooksPub) {
		
		books_P_List.add(BooksPub);
	}
 
}
