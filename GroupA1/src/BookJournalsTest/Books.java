package BookJournalsTest;


public class Books  {

	private int bookID;
	private String bookTitle;
	private int ISBN_B;
	private String Author_B;
	private int quantity;
	private double BookPrice;
	
	//JournalsPublisher T;
	//T = new JournalsPublisher();

	

	public Books(String bookTitle, int iSBN_B, String author_B,int quantity, double bookPrice) {
		
		this.bookTitle = bookTitle;
		this.ISBN_B = iSBN_B;
		this.Author_B = author_B;
		this.quantity = quantity;
		this.BookPrice = bookPrice;
	}

	

	public final int getBookID() {
		return bookID;
	}

	public final void setBookID(int book_ID) {
		this.bookID = book_ID;
	}



	public final String getbookTitle() {
		return bookTitle;
	}

	public final void setbookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public final int getISBN_B() {
		return ISBN_B;
	}

	public final void setISBN_B(int iSBN_B) {
		ISBN_B = iSBN_B;
	}
	public final String getAuthor_B() {
		return Author_B;
	}

	public final void setAuthor_B(String author_B) {
		Author_B = author_B;
	}
	
	public final int getQuantity() {
		return quantity;
	}

	public final void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public final double getbookPrice() {
		return BookPrice;
	}

	public final void setbookPrice(double bookPrice) {
		this.BookPrice = bookPrice;
	}
	
	
	

	


}
