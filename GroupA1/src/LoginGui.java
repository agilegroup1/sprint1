import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.*;
import javax.swing.border.*;

import java.sql.*;


public class LoginGui extends JFrame implements ActionListener{
	// DB Connectivity Attributess
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	
	JButton adminButton = new JButton("Admin Login");
	JButton studentButton = new JButton("Student Login");
	JButton registrationButton = new JButton("Student Registration");
	JPanel panel = new JPanel(); 
	JLabel titleLabel = new JLabel ("College Library Login");
	JLabel userLabel = new JLabel ("Username");
	JTextField username = new JTextField(15);
	JLabel passLabel = new JLabel ("Password");
	JPasswordField password = new JPasswordField(15);

	LoginGui(){
		super("College Library Maintence System");
		setEnabled(true);
		initiate_db_conn();
		setSize(400,250);
		setLocation(400,280);
		panel.setLayout (null); 

		titleLabel.setBounds(120,0,200,20);
		userLabel.setBounds(70,30,100,20);
		username.setBounds(170,30,100,20);
		passLabel.setBounds(70,50,100,20);
		password.setBounds(170,50,100,20);
		adminButton.setBounds(30,90,150,20);
		studentButton.setBounds(200,90,150,20);
		registrationButton.setBounds(120,120,150,20);

		panel.add(titleLabel);
		panel.add(adminButton);
		panel.add(studentButton);
		panel.add(userLabel);
		panel.add(passLabel);
		panel.add(username);
		panel.add(password);
		panel.add(registrationButton);

		adminButton.addActionListener(this);
		studentButton.addActionListener(this);
		registrationButton.addActionListener(this);

		getContentPane().add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		resetLogin();
	}

	public void initiate_db_conn()
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/Agile_Sprint1";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
			System.out.println("Connected");
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	
	


	//event handling 
	public void actionPerformed(ActionEvent e) 
	{
		Object target=e.getSource();
		if (target == adminButton)
		{
			try
			{
				String statement = "select * from admin_login where username='" +username.getText()+"';";

				rs = stmt.executeQuery(statement);
				rs.next();
				String user = rs.getString("username");
				String pass = rs.getString("passwords");
				String u = username.getText();
				String p = String.valueOf(password.getPassword());

				if((u.equals(user)) && ((p.equals(pass))))
				{
					//JOptionPane.showMessageDialog (null, "Admin Login Successful", "Admin Login", JOptionPane.INFORMATION_MESSAGE);
					JFrame AdminMenu = new JFrame();
					new AdminMenu();
					dispose();
					
					//resetLogin();
				}
				else
				{
					JOptionPane.showMessageDialog (null, "Admin Login Unsuccessful", "Admin Login", JOptionPane.INFORMATION_MESSAGE);
					resetLogin();
				}

			}catch (SQLException sqle){
				System.err.println("Error with  login:\n"+sqle.toString());}			
		}

		if (target == studentButton)
		{
			try
			{
				String statement = "select * from student_login where username='" +username.getText()+"';";

				rs = stmt.executeQuery(statement);
				rs.next();
				String user = rs.getString("username");
				String pass = rs.getString("passwords");			
				String u = username.getText();
				String p = String.valueOf(password.getPassword());

				if((u.equals(user)) && ((p.equals(pass))))
				{
					JFrame AdminMenu = new JFrame();
					new StudentMenu();
					dispose();
				}
				else
					JOptionPane.showMessageDialog(getParent(),
							"Student Login : Invalid Login\nAre you Registered, if not please use button below\n If registered, then try again");
				resetLogin();
				/*JOptionPane.showMessageDialog (null, "Not Student Login", "Student Login", JOptionPane.INFORMATION_MESSAGE);*/
			}catch (SQLException sqle){
				System.err.println("Error with  login:\n"+sqle.toString());}

		}


		if (target == registrationButton)
		{
			//JOptionPane.showMessageDialog(getParent(), "Student Registration Required");
			JFrame StudentReg = new JFrame();
			new StudentReg();
			resetLogin();
		}


	}

	private void resetLogin() {
		username.setText("");
		password.setText("");
		username.requestFocus();

	}
	
	public static void main(String[] args) 
	{
		LoginGui frameTabel = new LoginGui();
	}

}
