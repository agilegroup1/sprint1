
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StudentMenu extends JFrame implements ActionListener{
	
	String cmd = null;
	
	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	JPanel panel = new JPanel();
	JLabel titleLabel = new JLabel ("College Library Student Menu");
	JLabel searchLabel = new JLabel ("Search:");
	JLabel resultLabel = new JLabel ("Result:");
	JLabel isbnLabel = new JLabel ("ISBN:");
	JLabel rtitleLabel = new JLabel ("Title:");
	JLabel rtypeLabel = new JLabel ("Type:");
	JLabel authorLabel = new JLabel ("Author:");
	JLabel quantityLabel = new JLabel ("Quantity:");
	JLabel editionLabel = new JLabel ("Edition:");
	JLabel priceLabel = new JLabel ("Price:");
	JLabel pnameLabel = new JLabel ("Pulisher Name:");
	JLabel paddressLabel = new JLabel ("Publisher Address:");
	JLabel pcontactLabel = new JLabel ("Publisher Contact:");
	JButton bookSearchButton = new JButton("BookSearch");
	JButton journalSearchButton = new JButton("JournalSearch");
	JButton requestButton = new JButton("Request");
	JButton notifications = new JButton("Notifications");
	JTextField searchText = new JTextField();
	JTextField isbnText = new JTextField();
	JTextField titleText = new JTextField();
	JTextField typeText = new JTextField();
	JTextField authorText = new JTextField();
	JTextArea area = new JTextArea();
	
	
	
	public StudentMenu(){
		initiate_db_conn();
		setSize(400,650);
		setLocation(500,280);
		panel.setLayout (null);
		
		
		titleLabel.setBounds(110,0,200,20);
		resultLabel.setBounds(30,130,50,20);
		isbnLabel.setBounds(30,170,50,20);
		rtitleLabel.setBounds(30,210,50,20);
		rtypeLabel.setBounds(30,250,50,20);
		authorLabel.setBounds(30,290,50,20);
		quantityLabel.setBounds(30,330,50,20);
		editionLabel.setBounds(30,370,50,20);
		priceLabel.setBounds(30,410,50,20);
		pnameLabel.setBounds(30,450,100,20);
		paddressLabel.setBounds(30,490,120,20);
		pcontactLabel.setBounds(30,530,120,20);
		searchLabel.setBounds(30,45,50,20);
		searchText.setBounds(90,40,190,30);
		isbnText.setBounds(150,160,190,30);
		titleText.setBounds(150,200,190,30);
		typeText.setBounds(150,240,190,30);
		authorText.setBounds(150,280,190,30);
		area.setBounds(30,330,330,200);
		
		bookSearchButton.setBounds(60,80,120,30);
		journalSearchButton.setBounds(200,80,120,30);
		requestButton.setBounds(210,570,120,30);
		notifications.setBounds(60,570,120,30);

		
		panel.add(titleLabel);
		panel.add(resultLabel);
		panel.add(searchLabel);
		panel.add(isbnLabel);
		panel.add(rtitleLabel);
		panel.add(rtypeLabel);
		panel.add(authorLabel);
		
		panel.add(bookSearchButton);
		panel.add(journalSearchButton);
		panel.add(requestButton);
		panel.add(notifications);
		panel.add(searchText);
		panel.add(isbnText);
		panel.add(titleText);
		panel.add(typeText);
		panel.add(authorText);
		panel.add(area);
		
		
		bookSearchButton.addActionListener(this);
		journalSearchButton.addActionListener(this);
		requestButton.addActionListener(this);
		notifications.addActionListener(this);
		
		
		getContentPane().add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

	}
	
	
	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/Agile_Sprint1";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "Ron2393?");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			System.out.println("Connected");
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n"
					+ e.getMessage());
		}
	}
	
	

		
	

	
	public void actionPerformed(ActionEvent e)
	{
		Object target = e.getSource();
		if(target == this.bookSearchButton){
			try{
			String it = this.searchText.getText();
			
			cmd = "select * "+  "from book_details " + "where ISBNbook = '"  +it+"';";
			it = searchText.getText();
			
			rs = stmt.executeQuery(cmd);
			rs.next();
			System.out.println(rs);
			String is = rs.getString("ISBNbook");
			isbnText.setText(is);
			String bt = rs.getString("title");
			titleText.setText(bt);
			
			String ty = rs.getString("b_type");
			typeText.setText(ty);
			String au = rs.getString("author");
			authorText.setText(au);
			String qu = rs.getString("quantity");
			
		}catch(Exception e1){e1.printStackTrace();}

		}
		
		if(target == this.journalSearchButton){
			try{
			String it = this.searchText.getText();
			
			cmd = "select * "+  "from journal_details " + "where ISBN = '"  +it+"';";
			it = searchText.getText();
			
			rs = stmt.executeQuery(cmd);
			rs.next();
			System.out.println(rs);
			String is = rs.getString("ISBN");
			isbnText.setText(is);
			String bt = rs.getString("title");
			titleText.setText(bt);
			
			String ty = rs.getString("j_type");
			typeText.setText(ty);
			String au = rs.getString("author");
			authorText.setText(au);
			String qu = rs.getString("quantity");
			
		}catch(Exception e1){e1.printStackTrace();}

		} 
		if(target==requestButton){
			try {
				Request_Interface window = new Request_Interface();
				window.frame.setVisible(true);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
	}
	
	/*public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		if (target == bookSearchButton){
			try{
				String bookSear = "select * from addbooks;"; 
				
		    
			}catch(Exception sqle){
				System.err.println("Error with  insert:\n"+sqle.toString());
			}
				
			
		}
	}*/
	
	public static void main(String[] args) {
		new StudentMenu();
	}
}