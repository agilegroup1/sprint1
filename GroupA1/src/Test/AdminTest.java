package Test;
import static org.junit.Assert.*;
import org.junit.Test;
import junit.framework.TestCase;

public class AdminTest extends TestCase {
////////////////////////////////////////////////////////test day between 0 to 10///////////////////////////////////////////////////////////////////
@Test
public void testExpireDay10_0001() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0.0, ex.Expire(9), 0.1);

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
	}
}

@Test
public void testExpireDay10_0002() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0, ex.Expire(10), 0.1);

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
	}

}
public void testExpireDay10_0003() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		ex.Expire(11);
		
		fail("Exception expected .....");

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid day input", expireExceptionHandler.getMessage());
		}
	}
public void testExpireDay10_0004() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0, ex.Expire(0),0.01);
	
	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());}
	}
public void testExpireDay10_0005() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0, ex.Expire(5),0.01);
	

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
		}
}

public void testExpireDay10_0006() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0, ex.Expire(2),0.01);
	

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
		}
}
	
public void testExpireDay10_000() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0, ex.Expire(3),0.01);
	

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
		}
}
	
public void testExpireDay10_0008() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0, ex.Expire(7),0.01);
	

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
		}
}
	

///////////////////////////////////////////////////////////test day between 10 to 14///////////////////////////////////////////////////////////////////////
@Test
public void testExpireDay14_0001() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0.25, ex.Expire2(11),0.01);


	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
	}

}

@Test
public void testExpireDay14_0002() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
	ex.Expire2(10);


	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid day input", expireExceptionHandler.getMessage());
	}

}
public void testExpireDay14_0003() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0.50, ex.Expire2(12),0.01);


	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid value", expireExceptionHandler.getMessage());
	}

}
public void testExpireDay14_0004() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(1.00, ex.Expire2(14),0.01);


	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid value", expireExceptionHandler.getMessage());
	}

}
@Test
public void testExpireDay14_0005() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
	ex.Expire2(15);


	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid day input", expireExceptionHandler.getMessage());
	}

}
public void testExpireDay14_0006() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(0.75, ex.Expire2(13),0.01);


	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid value", expireExceptionHandler.getMessage());
	}

}
/////////////////////////////////////////////////////test day between 14 to 30/////////////////////////////////////////////////////////////////////

public void testExpireAFDay14_0001() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		assertEquals(1.50, ex.Expire3(15),0.01);
		// int sum = ex.Expire(5, 10);
	

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
	}
}

public void testExpireAFDay14_0002() {

	// Need to create an Expire Object

	Admin ex = new Admin();

	try {
		ex.Expire3(14);
		// int sum = ex.Expire(5, 10);
		fail("Exception expected .....");

	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid day input", expireExceptionHandler.getMessage());

	}
}

public void testExpireAFDay14_0003() {
Admin ex = new Admin();

	try {
		assertEquals(8.50, ex.Expire3(29),0.01);
		
	} catch (ExpireExceptionHandler expireExceptionHandler) {

		assertSame("Invalid Values", expireExceptionHandler.getMessage());
	}

}


	public void testExpireAFDay14_0004() {

		// Need to create an Expire Object

		Admin ex = new Admin();

		try {
			assertEquals(1.50, ex.Expire3(15));
		

		} catch (ExpireExceptionHandler expireExceptionHandler) {

			assertSame("Invalid Values", expireExceptionHandler.getMessage());

		}
	}

	public void testExpireAFDay14_0005() {

		// Need to create an Expire Object

		Admin ex = new Admin();

		try {
			assertEquals(9, ex.Expire3(30),0.01);

		} catch (ExpireExceptionHandler expireExceptionHandler) {

			assertSame("Invalid Values", expireExceptionHandler.getMessage());}
		}

		public void testExpireAFDay14_0006() {

			// Need to create an Expire Object

			Admin ex = new Admin();

			try {
				ex.Expire3(31);
				fail("Exception expected .....");

			} catch (ExpireExceptionHandler expireExceptionHandler) {

				assertSame("Invalid day input", expireExceptionHandler.getMessage());

			}
		}

		public void testExpireAFDay14_0007() {

			// Need to create an Expire Object

			Admin ex = new Admin();

			try {
				assertEquals(3, ex.Expire3(18),0.01);
				// int sum = ex.Expire(5, 10);
			

			} catch (ExpireExceptionHandler expireExceptionHandler) {

				assertSame("Invalid Values", expireExceptionHandler.getMessage());}
			}

				public void testExpireAFDay14_0008() {

					// Need to create an Expire Object

					Admin ex = new Admin();

					try {
						ex.Expire5(-1);
						// int sum = ex.Expire(5, 10);
						fail("Exception expected .....");

					} catch (ExpireExceptionHandler expireExceptionHandler) {

						assertSame("That impossible..............", expireExceptionHandler.getMessage());}
					}


////////////////////////////////////////////////////////test days after 30///////////////////////////////////////////////////////////////////

				public void testExpireDay30_0001() {

					// Need to create an Expire Object

					Admin ex = new Admin();

					try {
						ex.Expire6(30);
						// int sum = ex.Expire(5, 10);
						fail("Exception expected .....");
					

					} catch (ExpireExceptionHandler expireExceptionHandler) {

						assertSame("Invalid day input", expireExceptionHandler.getMessage());

					}
				}
				
				public void testExpireDay30_0002() {

					// Need to create an Expire Object

					Admin ex = new Admin();

					try {
						ex.Expire6(31);
					
					} catch (ExpireExceptionHandler expireExceptionHandler) {

						assertSame("Invalid day input", expireExceptionHandler.getMessage());}
				}
				public void testExpireDay30_0003() {

					// Need to create an Expire Object

					Admin ex = new Admin();

					try {
						ex.Expire6(32);
					
					} catch (ExpireExceptionHandler expireExceptionHandler) {

						assertSame("Invalid day input", expireExceptionHandler.getMessage());}

					}
				
				public void testExpireDay30_0004() {

					// Need to create an Expire Object

					Admin ex = new Admin();

					try {
						ex.Expire6(33);
					
					} catch (ExpireExceptionHandler expireExceptionHandler) {

						assertSame("Invalid day input", expireExceptionHandler.getMessage());}

						}


public void testExpireDay30_0005() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(34);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}
}
public void testExpireDay30_0006() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(35);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}

	}

public void testExpireDay30_0007() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(36);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}

	}

public void testExpireDay30_0008() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(37);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}
}
public void testExpireDay30_0009() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(38);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}

	}

public void testExpireDay30_0010() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(39);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}

	}


public void testExpireDay30_0011() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(40);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}

	}

public void testExpireDay30_0012() {

	// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(41);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}
}


public void testExpireDay30_0013() {

// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(42);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}

}

public void testExpireDay30_0014() {

// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(43);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}
	}


public void testExpireDay30_0015() {

// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(44);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}

}

public void testExpireDay30_0016() {

// Need to create an Expire Object

Admin ex = new Admin();

try {
	ex.Expire6(45);

} catch (ExpireExceptionHandler expireExceptionHandler) {

	assertSame("Invalid day input", expireExceptionHandler.getMessage());}
	}
}

