import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.*;
import javax.swing.border.*;

import java.sql.*;

public class ViewRequest extends JInternalFrame implements ActionListener
{	
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private JPanel teamsPanel;
	
	//private JPanel exportConceptDataPanel;
	private JScrollPane dbContentsPanel;

	private Border lineBorder;

	private JLabel IDLabel=new JLabel("Request ID:                 ");
	private JLabel ISBNLabel=new JLabel("Item ISBN:               ");
	private JLabel TitleLabel=new JLabel("Title:      ");
	private JLabel AuthorLabel=new JLabel("Author:      ");
	private JLabel re_byLabel=new JLabel("request by:      ");

	
	private JTextField IDTF= new JTextField(10);
	private JTextField ISBN_TF=new JTextField(10);
	private JTextField Title_TF=new JTextField(10);
	private JTextField Author_TF=new JTextField(10);
	private JTextField re_by_TF=new JTextField(10);

	


	private static requestsQueryTable TableModel = new requestsQueryTable();
	//Add the models to JTabels
	private JTable TableofDBContents=new JTable(TableModel);
	//Buttons for inserting, and updating members
	//also a clear button to clear players panel
	private JButton updateButton = new JButton("Update");
	private JButton insertButton = new JButton("Insert");
	private JButton exportButton  = new JButton("Export");
	private JButton deleteButton  = new JButton("Delete");
	private JButton clearButton  = new JButton("Clear");
	



	public ViewRequest( String aTitle)
	{	
		//setting up the GUI
		super(aTitle, false,false,false,false);
		setEnabled(true);

		initiate_db_conn();
		//add the 'main' panel to the Internal Frame
		content=getContentPane();
		content.setLayout(null);
		content.setBackground(Color.lightGray);
		lineBorder = BorderFactory.createEtchedBorder(15, Color.red, Color.black);

		//setup details panel and add the components to it
		teamsPanel=new JPanel();
		teamsPanel.setLayout(new GridLayout(11,2));
		teamsPanel.setBackground(Color.lightGray);
		teamsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "CRUD Actions"));

		teamsPanel.add(IDLabel);			
		teamsPanel.add(IDTF);
		teamsPanel.add(ISBNLabel);		
		teamsPanel.add(ISBN_TF);
		teamsPanel.add(TitleLabel);
		teamsPanel.add(Title_TF);	
		teamsPanel.add(AuthorLabel);
		teamsPanel.add(Author_TF);
		teamsPanel.add(re_byLabel);
		teamsPanel.add(re_by_TF);	

		

		//setup details panel and add the components to it

		insertButton.setSize(100, 30);
		updateButton.setSize(100, 30);
		exportButton.setSize (100, 30);
		deleteButton.setSize (100, 30);
		clearButton.setSize (100, 30);
		

		insertButton.setLocation(370, 5);
		updateButton.setLocation(370, 40);
		deleteButton.setLocation (370, 75);
		exportButton.setLocation (370, 110);		
		clearButton.setLocation (370, 145);
		
		
		insertButton.addActionListener(this);
		updateButton.addActionListener(this);
		exportButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);

		content.add(insertButton);
		content.add(updateButton);
		content.add(exportButton);
		content.add(deleteButton);
		content.add(clearButton);
		

		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel=new JScrollPane(TableofDBContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(Color.lightGray);
		dbContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder,"Request items Database"));

		teamsPanel.setSize(360, 300);
		teamsPanel.setLocation(3,0);
		dbContentsPanel.setSize(700, 300);
		dbContentsPanel.setLocation(477, 0);

		content.add(teamsPanel);
		content.add(dbContentsPanel);

		setSize(982,645);
		setVisible(true);
		

		TableModel.refreshFromDB(stmt);
	}

	public void initiate_db_conn()
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/Agile_Sprint1";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}

	//event handling 
	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		if (target == clearButton)
		{
			clear();

		}
		
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		if (target == insertButton)
		{		 
			try
			{
				String updateTemp ="INSERT INTO requests VALUES("+
				null +",'"+
				ISBN_TF.getText()+"','"+
				Title_TF.getText()+"','"+
				Author_TF.getText()+"','"+
				re_by_TF.getText()+"');";
				System.out.println(updateTemp);
				stmt.executeUpdate(updateTemp);
				clear();

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  insert:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		if (target == deleteButton)
		{

			try
			{
				 
				String updateTemp = "DELETE FROM requests WHERE id = " + IDTF.getText() + ";";
				stmt.executeUpdate(updateTemp);
				clear();
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with delete:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		if (target == updateButton)
		{	 	
			try
			{ 			
				String updateTemp ="UPDATE requests SET " +
				"ISBN = '"+ISBN_TF.getText()+
				"', item_title = '"+Title_TF.getText()+
				"', item_author = "+Author_TF.getText()+
				", item_request_by ='"+re_by_TF.getText()+
				" where id = "+IDTF.getText();


				stmt.executeUpdate(updateTemp);
				//these lines do nothing but the table updates when we access the db.
				rs = stmt.executeQuery("SELECT * from requests ");
				rs.next();
				rs.close();	
			}
			catch (SQLException sqle){
				System.err.println("Error with  update:\n"+sqle.toString());
			}
			finally{
				TableModel.refreshFromDB(stmt);
			}
		}
		
		
		if (target == exportButton)
		{	 	
			try
			{ 			
				rs = stmt.executeQuery("SELECT * from requests ");
				writeToFile(rs);
				rs.next();
				rs.close();	
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  update:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
	}
		
		/////////////////////////////////////////////////////////////////////////////////////
		
	

	private void writeToFile(ResultSet rs){
		try{
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("RequestItem.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();}
	}
	
	public void clear(){
		IDTF.setText("");
		ISBN_TF.setText("");
		Title_TF.setText("");
		Author_TF.setText("");
		re_by_TF.setText("");
		
		
	}
}
