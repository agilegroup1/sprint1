import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class View extends JFrame implements ActionListener{

	
	// DB Connectivity Attributes
			private Connection con = null;
			private Statement stmt = null;
			private ResultSet rs = null;

		
			JLabel titleLabel = new JLabel ("College Library View Book/Journal");
			
			JLabel viewLabel = new JLabel ("Enter Book/Journal IBSN No for Viewing: ");
			
			JTextField viewisbn = new JTextField(12);
			JButton viewButton = new JButton("Search for ISBN No");
			JPanel panel = new JPanel();

			View(){
				super("College Library");
				setSize(500,900);
				setLocation(400,280);
				panel.setLayout (null); 

				titleLabel.setBounds(120,0,200,20);
				viewLabel.setBounds(30,30,250,20);
				viewisbn.setBounds(270,30,100,20);
				
				viewButton.setBounds(190,60,160,30);
				
			
				
				panel.add(titleLabel);
				panel.add(viewLabel);
				panel.add(viewisbn);
				panel.add(viewButton);	
			
			
				viewButton.addActionListener(this);

			getContentPane().add(panel);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setVisible(true);
		}
		
		//event handling 
			public void actionPerformed(ActionEvent e)
			{
				Object target=e.getSource();
				if (target == viewButton)
				{
					new AdminMenu();
					dispose();	
				}
			}
		
		public static void main(String[] args) {
			JFrame Menu = new JFrame();
			}

	}
