import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;

@SuppressWarnings("serial")
public class BooksWindowContent extends JInternalFrame implements ActionListener {
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private JPanel detailsPanel;
	private JPanel exportButtonPanel;
	// private JPanel exportConceptDataPanel;
	private JScrollPane dbContentsPanel;

	private Border lineBorder;

	private JLabel IDLabel = new JLabel("ID no:                 ");
	private JLabel B_ISBNLabel = new JLabel("ISBN no:                 ");
	private JLabel B_Title_Label = new JLabel("	Book Title:               ");
	private JLabel B_Type_Label = new JLabel("Book type:      ");
	private JLabel B_Author_Label = new JLabel("Author:        ");
	private JLabel B_Quantity_Label = new JLabel("Quantity:                 ");
	private JLabel B_Edition_Label = new JLabel("Edition:               ");
	private JLabel B_Price_Label = new JLabel("Price:      ");
	private JLabel B_publisherN_Label = new JLabel("Publisher Name:      ");

	private JLabel B_publisherA_Label = new JLabel("Publisher Address:      ");
	private JLabel B_publisherC_Label = new JLabel("Publisher contact:        ");

	private JTextField IDTF = new JTextField(10);
	private JTextField B_ISBN_TF = new JTextField(10);
	private JTextField B_Title_TF = new JTextField(10);
	private JTextField B_Type_TF = new JTextField(10);
	private JTextField B_Author_TF = new JTextField(10);
	private JTextField B_Quantity_TF = new JTextField(10);
	private JTextField B_Edition_TF = new JTextField(10);
	private JTextField B_Price_TF = new JTextField(10);
	private JTextField B_publisherN_TF = new JTextField(10);
	private JTextField B_publisherA_TF = new JTextField(10);
	private JTextField B_publisherC_TF = new JTextField(10);

	private static QueryTableModel TableModel = new QueryTableModel();
	// Add the models to JTabels
	private JTable TableofDBContents = new JTable(TableModel);
	// Buttons for inserting, and updating members
	// also a clear button to clear details panel
	private JButton updateButton = new JButton("Update");
	private JButton addButton = new JButton("Add");
	private JButton viewButton = new JButton("View");
	private JButton deleteButton = new JButton("Delete");
	private JButton clearButton = new JButton("Clear");
	private JButton nextButton = new JButton("Next");
	private JButton prevButton = new JButton("Prev");

	int current = 0;

	public BooksWindowContent(String aTitle) {
		// setting up the GUI
		super(aTitle, false, false, false, false);
		setEnabled(true);

		initiate_db_conn();
		// add the 'main' panel to the Internal Frame
		content = getContentPane();
		content.setLayout(null);
		content.setBackground(Color.DARK_GRAY);
		lineBorder = BorderFactory.createEtchedBorder(15, Color.red, Color.black);

		// setup details panel and add the components to it
		detailsPanel = new JPanel();
		detailsPanel.setLayout(new GridLayout(11, 2));
		detailsPanel.setBackground(Color.white);
		detailsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "CRUD Actions"));

		detailsPanel.add(IDLabel);
		detailsPanel.add(IDTF);
		detailsPanel.add(B_ISBNLabel);
		detailsPanel.add(B_ISBN_TF);
		detailsPanel.add(B_Title_Label);
		detailsPanel.add(B_Title_TF);
		detailsPanel.add(B_Type_Label);
		detailsPanel.add(B_Type_TF);
		detailsPanel.add(B_Author_Label);
		detailsPanel.add(B_Author_TF);
		detailsPanel.add(B_Quantity_Label);
		detailsPanel.add(B_Quantity_TF);
		detailsPanel.add(B_Edition_Label);
		detailsPanel.add(B_Edition_TF);
		detailsPanel.add(B_Price_Label);
		detailsPanel.add(B_Price_TF);
		detailsPanel.add(B_publisherN_Label);
		detailsPanel.add(B_publisherN_TF);
		detailsPanel.add(B_publisherA_Label);
		detailsPanel.add(B_publisherA_TF);
		detailsPanel.add(B_publisherC_Label);
		detailsPanel.add(B_publisherC_TF);

		addButton.setSize(100, 20);
		updateButton.setSize(100, 20);
		viewButton.setSize(100, 20);
		deleteButton.setSize(100, 20);
		clearButton.setSize(100, 20);
		nextButton.setSize(100, 20);
		prevButton.setSize(100, 20);

		addButton.setLocation(370, 10);
		updateButton.setLocation(370, 90);
		viewButton.setLocation(370, 130);
		deleteButton.setLocation(370, 50);
		clearButton.setLocation(370, 170);
		nextButton.setLocation(370, 210);
		prevButton.setLocation(370, 250);

		addButton.addActionListener(this);
		updateButton.addActionListener(this);
		viewButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);
		nextButton.addActionListener(this);
		prevButton.addActionListener(this);

		content.add(addButton);
		content.add(updateButton);
		content.add(viewButton);
		content.add(deleteButton);
		content.add(clearButton);
		content.add(nextButton);
		content.add(prevButton);

		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel = new JScrollPane(TableofDBContents, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(Color.lightGray);
		dbContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Database Content"));

		detailsPanel.setSize(360, 300);
		detailsPanel.setLocation(3, 0);
		dbContentsPanel.setSize(700, 300);
		dbContentsPanel.setLocation(477, 0);

		content.add(detailsPanel);
		content.add(dbContentsPanel);

		setSize(982, 645);
		setVisible(true);

		TableModel.refreshFromDB(stmt);
	}

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/Agile_Sprint1";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "Ron2393?");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
	}

	// event handling
	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();
		if (target == clearButton) {
			IDTF.setText("");
			B_ISBN_TF.setText("");
			B_Title_TF.setText("");
			B_Type_TF.setText("");
			B_Author_TF.setText("");
			B_Quantity_TF.setText("");
			B_Edition_TF.setText("");
			B_Price_TF.setText("");
			B_publisherN_TF.setText("");
			B_publisherA_TF.setText("");
			B_publisherC_TF.setText("");

		}

		if (target == addButton) {
			try {

				String updateTemp = "INSERT INTO book_details VALUES(" + null + ",'" + B_ISBN_TF.getText() + "','"
						+ B_Title_TF.getText() + "','" + B_Type_TF.getText() + "','" + B_Author_TF.getText() + "',"
						+ B_Quantity_TF.getText() + ",'" + B_Edition_TF.getText() + "','" + B_Price_TF.getText() + "','"
						+ B_publisherN_TF.getText() + "','" + B_publisherA_TF.getText() + "',"
						+ B_publisherC_TF.getText() + ");";

				stmt.executeUpdate(updateTemp);

			} catch (SQLException sqle) {
				System.err.println("Error with  insert:\n" + sqle.toString());
			} finally {
				TableModel.refreshFromDB(stmt);
			}
		}

		if (target == deleteButton) {

			try {
				String updateTemp = "DELETE FROM book_details WHERE id = " + IDTF.getText() + ";";
				stmt.executeUpdate(updateTemp);

			} catch (SQLException sqle) {
				System.err.println("Error with delete:\n" + sqle.toString());
			} finally {
				TableModel.refreshFromDB(stmt);
			}
		}

		if (target == nextButton) {
			current++;
			try {
				String updateTemp = "SELECT * FROM book_details WHERE id = " + current + ";";
				PreparedStatement stmt = con.prepareStatement(updateTemp);
				ResultSet rs = stmt.executeQuery(updateTemp);
				if (rs.next()) {
					String id = rs.getString(1);
					IDTF.setText(id);
					String bisbn = rs.getString(2);
					B_ISBN_TF.setText(bisbn);
					String btitle = rs.getString(3);
					B_Title_TF.setText(btitle);
					String btype = rs.getString(4);
					B_Type_TF.setText(btype);
					String bauthor = rs.getString(5);
					B_Author_TF.setText(bauthor);
					String bquantity = rs.getString(6);
					B_Quantity_TF.setText(bquantity);
					String bedition = rs.getString(7);
					B_Edition_TF.setText(bedition);
					String bprice = rs.getString(8);
					B_Price_TF.setText(bprice);
					String bpubname = rs.getString(9);
					B_publisherN_TF.setText(bpubname);
					String bpubadd = rs.getString(10);
					B_publisherA_TF.setText(bpubadd);
					String bpubcon = rs.getString(11);
					B_publisherC_TF.setText(bpubcon);
				}
			} catch (SQLException sqle) {
				System.err.println("Error with next \n");
			}
		}

		if (target == prevButton) {
			current--;
			try {
				String updateTemp = "SELECT * FROM book_details WHERE id = " + current + ";";
				PreparedStatement stmt = con.prepareStatement(updateTemp);
				ResultSet rs = stmt.executeQuery(updateTemp);
				if (rs.next()) {
					String id = rs.getString(1);
					IDTF.setText(id);
					String bisbn = rs.getString(2);
					B_ISBN_TF.setText(bisbn);
					String btitle = rs.getString(3);
					B_Title_TF.setText(btitle);
					String btype = rs.getString(4);
					B_Type_TF.setText(btype);
					String bauthor = rs.getString(5);
					B_Author_TF.setText(bauthor);
					String bquantity = rs.getString(6);
					B_Quantity_TF.setText(bquantity);
					String bedition = rs.getString(7);
					B_Edition_TF.setText(bedition);
					String bprice = rs.getString(8);
					B_Price_TF.setText(bprice);
					String bpubname = rs.getString(9);
					B_publisherN_TF.setText(bpubname);
					String bpubadd = rs.getString(10);
					B_publisherA_TF.setText(bpubadd);
					String bpubcon = rs.getString(11);
					B_publisherC_TF.setText(bpubcon);
				}
			} catch (SQLException sqle) {
				System.err.println("Error with previous \n");
			}
		}

		if (target == viewButton) {
			String updateTemp = "SELECT id, ISBNbook, title, b_type, author, quantity,"
					+ " edition, price, publisherName, publisherAddress, publisherContact FROM book_details WHERE id = "
					+ IDTF.getText() + ";";

			try {
				PreparedStatement stmt = con.prepareStatement(updateTemp);
				ResultSet rs = stmt.executeQuery(updateTemp);
				while (rs.next()) {
					String bisbn = rs.getString(2);
					B_ISBN_TF.setText(bisbn);
					String btitle = rs.getString(3);
					B_Title_TF.setText(btitle);
					String btype = rs.getString(4);
					B_Type_TF.setText(btype);
					String bauthor = rs.getString(5);
					B_Author_TF.setText(bauthor);
					String bquantity = rs.getString(6);
					B_Quantity_TF.setText(bquantity);
					String bedition = rs.getString(7);
					B_Edition_TF.setText(bedition);
					String bprice = rs.getString(8);
					B_Price_TF.setText(bprice);
					String bpubname = rs.getString(9);
					B_publisherN_TF.setText(bpubname);
					String bpubadd = rs.getString(10);
					B_publisherA_TF.setText(bpubadd);
					String bpubcon = rs.getString(11);
					B_publisherC_TF.setText(bpubcon);
				}
			} catch (SQLException sqle) {
				System.err.println("Error with delete:\n" + sqle.toString());
			}
			current = Integer.parseInt(IDTF.getText());
		}

		if (target == updateButton) {
			try {
				String updateTemp = "UPDATE book_details SET " + "ISBNbook = '" + B_ISBN_TF.getText() + "', title = '"
						+ B_Title_TF.getText() + "', b_type = '" + B_Type_TF.getText() + "', author = '"
						+ B_Author_TF.getText() + "', quantity =" + B_Quantity_TF.getText() + ", edition = '"
						+ B_Edition_TF.getText() + "', price = '" + B_Price_TF.getText() + "', publisherName = '"
						+ B_publisherN_TF.getText() + "', publisherAddress = '" + B_publisherA_TF.getText()
						+ "', publisherContact = " + B_publisherC_TF.getText() + " where id = " + IDTF.getText() + ";";

				stmt.executeUpdate(updateTemp);
				// these lines do nothing but the table updates when we access
				// the db.
				rs = stmt.executeQuery("SELECT * from book_details ");
				rs.next();
				rs.close();
			} catch (SQLException sqle) {
				System.err.println("Error with  update:\n" + sqle.toString());
			} finally {
				TableModel.refreshFromDB(stmt);
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// I have only added functionality of 2 of the button on the lower right of
	///////////////////////////////////////////////////////////////////////////////////// the
	///////////////////////////////////////////////////////////////////////////////////// template
	///////////////////////////////////////////////////////////////////////////////////

	/*
	 * private void writeToFile(ResultSet rs){ try{
	 * System.out.println("In writeToFile"); FileWriter outputFile = new
	 * FileWriter("Ahmed.csv"); PrintWriter printWriter = new
	 * PrintWriter(outputFile); ResultSetMetaData rsmd = rs.getMetaData(); int
	 * numColumns = rsmd.getColumnCount();
	 * 
	 * for(int i=0;i<numColumns;i++){
	 * printWriter.print(rsmd.getColumnLabel(i+1)+","); }
	 * printWriter.print("\n"); while(rs.next()){ for(int i=0;i<numColumns;i++){
	 * printWriter.print(rs.getString(i+1)+","); } printWriter.print("\n");
	 * printWriter.flush(); } printWriter.close(); } catch(Exception
	 * e){e.printStackTrace();} }
	 */
}
