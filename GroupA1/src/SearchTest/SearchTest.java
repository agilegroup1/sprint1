package SearchTest;
import static org.junit.Assert.*;

import org.junit.Test;

public class SearchTest {

	@Test
	    //test1
		//object��test the do we have this book in the library
		//Input(s):book1
		//Expection Outpute:true
	public void test() {
		Admin Book=new Admin();
		try{
			assertEquals(true,Book.Dowehavethisbook("book1"));
		}
		catch(Exception e){
			fail("Not yet Implement");
		}
		
	}
	@Test
	 //test5
	//object��test the do we have this book in the library
	//Input(s):bookname="book1" quantity=5
	//Expection Outpute:true
	public void test5() {
		Admin Book=new Admin();
		try{
			assertEquals(true,Book.afterConnectionDBbook("book1",5));
		}
		catch(Exception e){
			fail("Not yet Implement");
		}
		
	}
	@Test
	    //test5
		//object��test the do we have this book in the library
		//Input(s):bookname="book1" but quantity=0
		//Expection Outpute:false
	public void test6() {
		Admin Book=new Admin();
		try{
			Book.afterConnectionDBbook("book1",0);
		}
		catch(Exception e){
			assertSame("The book quantity equal to zero",e.getMessage());
		}
		
	}

	@Test
	//test2
	//object:test does the journal1 exist or not
	//Input(s): journal1
	//Exception output: true;
	public void test2(){
		Admin Journal= new Admin();
		try{
			assertEquals(true,Journal.DowehavethisJournal("journal1"));
		}
		catch(Exception e){
			fail("Not yet Implement");
		}
	}
	@Test
	    //test7
		//object:test does the journal1 exist or not
		//Input(s): journal1,0
		//Exception output:The journal quantity equal to zero;
    public void test7() {
	Admin Book=new Admin();
	try{
		Book.afterConnectionDBjournal("journal1",0);
	}
	catch(Exception e){
		assertSame("The journal quantity equal to zero",e.getMessage());
	}
	
}
	@Test
	//test8
		//object:test does the journal1 exist or not
		//Input(s): journal1,5
		//Exception output: true;
    public void test8() {
	Admin Book=new Admin();
	try{
		assertEquals(true,Book.afterConnectionDBjournal("journal1",5));
	}
	catch(Exception e){
		fail("Not yet Implement");
	}
	
}
	@Test
	//test3
	//object:test does the bname1 exist or not
	//Input(s): bname1
	//Exception output: true;
	public void test3(){
		Student Book=new Student();
		try{
			assertEquals(true,Book.Doesthebookexist("bname1"));
		}catch(Exception e){
			fail("Sorry, the book is not exist");
		}
	}
	@Test
	//test4
	//object:test does the jname1 exist or not
	//Input(s): jname1
	//Exception output: true;
	public void test4(){
		Student Journal=new Student();
		try{
			assertEquals(true,Journal.Doesthejournalexist("jname1"));
		}catch(Exception e){
			fail("Sorry, the journal is not exist");
		}
	}
	@Test
	 //test9
	//object��test the do we have this book in the library
	//Input(s):bookname="book1" quantity=5
	//Expection Outpute:true
	public void test9() {
		Student Book=new Student();
		try{
			assertEquals(true,Book.afterConnectionDBbook("book1",5));
		}
		catch(Exception e){
			fail("Not yet Implement");
		}
		
	}
	@Test
	 //test10
	//object��test the do we have this book in the library
	//Input(s):bookname="book1" quantity=0
	//Expection Outpute:false
	public void test10() {
		Student Book=new Student();
		try{
			assertEquals(false,Book.afterConnectionDBbook("book1",0));
		}
		catch(Exception e){
			fail("Not yet Implement");
		}
	}
	@Test
	 //test11
	//object��test the do we have this journal in the library
	//Input(s):journalname="jname1" quantity=0
	//Expection Outpute:false
	public void test11() {
		Student Journal=new Student();
		try{
			assertEquals(false,Journal.afterConnectionDBjournal("jname1",0));
		}catch(Exception e){
			fail("Not yet Implement");
		}
	
	}
	@Test
	 //test12
	//object��test the do we have this journal in the library
	//Input(s):journalname="jname1" quantity=0
	//Expection Outpute:false
	public void test12() {
		Student Journal=new Student();
		try{
			assertEquals(true,Journal.afterConnectionDBjournal("jname1",5));
		}catch(Exception e){
			fail("Not yet Implement");
		}
	
	}
	
}
