import javax.swing.*;
import javax.swing.table.TableModel;

import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RequestOtherLib extends JFrame implements ActionListener{
	
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	
	
	
	JLabel titleLabel = new JLabel ("Search book from other library");
	JLabel viewLabel = new JLabel ("Enter Book IBSN No to search for: ");
	
	JLabel resultLabel = new JLabel ("Result:");
	JLabel risbnLabel = new JLabel ("ISBN:");
	JLabel rtitleLabel = new JLabel ("Title:");
	JLabel rtypeLabel = new JLabel ("Type:");
	JLabel authorLabel = new JLabel ("Author:");
	JLabel quantityLabel = new JLabel ("Quantity:");
	JLabel editionLabel = new JLabel ("Edition:");
	JLabel priceLabel = new JLabel ("Price:");
	JLabel pnameLabel = new JLabel ("Pulisher Name:");
	JLabel paddressLabel = new JLabel ("Publisher Address:");
	JLabel pcontactLabel = new JLabel ("Publisher Contact:");
	
	JTextField isbnText = new JTextField();
	JTextField risbnText = new JTextField();
	JTextField titleText = new JTextField();
	JTextField typeText = new JTextField();
	JTextField authorText = new JTextField();
	JTextField quantityText = new JTextField();
	JTextField editionText = new JTextField();
	JTextField priceText = new JTextField();
	JTextField pnameText = new JTextField();
	JTextField paddressText = new JTextField();
	JTextField pcontactText = new JTextField();
	JTextArea text = new JTextArea();
	JButton getButton = new JButton("Get all books");
	JButton menuButton = new JButton("Return to menu");
	JButton searchButton = new JButton("Search");
	JButton requestButton = new JButton("Request");
	
	JPanel panel = new JPanel();
	
	int current = 0;
	
	
	RequestOtherLib(){
		initiate_conn();
		setSize(750,700);
		setLocation(10,10);
		panel.setLayout (null); 

		
		titleLabel.setBounds(260,30,250,20);
		viewLabel.setBounds(360,90,250,20);
		resultLabel.setBounds(360,130,50,20);
		risbnLabel.setBounds(360,170,50,20);
		rtitleLabel.setBounds(360,210,50,20);
		rtypeLabel.setBounds(360,250,50,20);
		authorLabel.setBounds(360,290,50,20);
		quantityLabel.setBounds(360,330,50,20);
		editionLabel.setBounds(360,370,50,20);
		priceLabel.setBounds(360,410,50,20);
		pnameLabel.setBounds(360,450,100,20);
		paddressLabel.setBounds(360,490,120,20);
		pcontactLabel.setBounds(360,530,120,20);
		
		isbnText.setBounds(580,90,100,30);
		risbnText.setBounds(490,160,190,30);
		titleText.setBounds(490,200,190,30);
		typeText.setBounds(490,240,190,30);
		authorText.setBounds(490,280,190,30);
		quantityText.setBounds(490,320,190,30);
		editionText.setBounds(490,360,190,30);
		priceText.setBounds(490,400,190,30);
		pnameText.setBounds(490,440,190,30);
		paddressText.setBounds(490,480,190,30);
		pcontactText.setBounds(490,520,190,30);
		text.setBounds(50,120,260,460);
		
		getButton.setBounds(100,70,160,30);
		menuButton.setBounds(100,600,160,30);
		searchButton.setBounds(360, 600, 130, 30);
		requestButton.setBounds(530,600,130,30);
		
	
		panel.add(risbnLabel);
		panel.add(titleLabel);
		panel.add(viewLabel);
		panel.add(resultLabel);
		panel.add(risbnLabel);
		panel.add(rtitleLabel);
		panel.add(rtypeLabel);
		panel.add(authorLabel);
		panel.add(quantityLabel);
		panel.add(editionLabel);
		panel.add(priceLabel);
		panel.add(pnameLabel);
		panel.add(paddressLabel);
		panel.add(pcontactLabel);
		
		panel.add(isbnText);
		panel.add(risbnText);
		panel.add(titleText);
		panel.add(typeText);
		panel.add(authorText);
		panel.add(quantityText);
		panel.add(editionText);
		panel.add(priceText);
		panel.add(pnameText);
		panel.add(paddressText);
		panel.add(pcontactText);
		panel.add(text);
		panel.add(getButton);
		panel.add(menuButton);
		panel.add(searchButton);
		panel.add(requestButton);
		
		
	
		getButton.addActionListener(this);
		menuButton.addActionListener(this);
		searchButton.addActionListener(this);
		requestButton.addActionListener(this);

		getContentPane().add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

	}
	
	
	public void initiate_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/NewlibraryBook";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			System.out.println("Connected");
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n"
					+ e.getMessage());
		}
	}
	
	
	
	
	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		
		if (target == menuButton)
		{
			
				dispose();
				//JFrame AdminMenu = new JFrame();
				new AdminMenu();
				
		}
		
		if (target == getButton)
		{
			//System.out.println("Searching" + it);
			String cmd = "SELECT ISBN, bookTitle FROM Newlibrary;";
			//it = isbnText.getText();
			System.out.println("Getting");
			try{
				PreparedStatement stmt = con.prepareStatement(cmd);
				ResultSet rs = stmt.executeQuery(cmd);
				System.out.println(rs);
				
				while (rs.next()) {
					String is1 = rs.getString(1);
					String is2 = rs.getString(2);
					String iss = is1+is2;
					System.out.println(iss);
					
					text.setText(text.getText()+iss+"\n");
				}
				
			}catch(Exception e0){
				JOptionPane.showMessageDialog (null, "Search Unsuccessful - book not found", "Search", JOptionPane.INFORMATION_MESSAGE);
				new RequestOtherLib();
			}
		}
		
		if (target == searchButton)
		{
			String it = isbnText.getText();
			//System.out.println("Searching" + it);
			String cmd = "SELECT ISBN, bookTitle, bookType, author, quantity,"
					+ " edition, price, publisherName, publisherEmail, publisherPhone FROM Newlibrary WHERE ISBN = "
					+ it + ";";
			//it = isbnText.getText();
			System.out.println("Searching" + it);
			try
			{
			PreparedStatement stmt = con.prepareStatement(cmd);
			ResultSet rs = stmt.executeQuery(cmd);
			System.out.println(rs);
			while (rs.next()) {
				String is = rs.getString(1);
				risbnText.setText(is);
				System.out.println("Searching" + is);
				String bt = rs.getString(2);
				titleText.setText(bt);					
				String ty = rs.getString(3);
				typeText.setText(ty);
				String au = rs.getString(4);
				authorText.setText(au);
				String qu = rs.getString(5);
				quantityText.setText(qu);
				String ed = rs.getString(6);
				editionText.setText(ed);
				String pr = rs.getString(7);
				priceText.setText(pr);
				String pn = rs.getString(8);
				pnameText.setText(pn);
				String pa = rs.getString(9);
				paddressText.setText(pa);
				String pc = rs.getString(10);
				pcontactText.setText(pc);
			}
			System.out.println(rs);
		}catch(Exception e1){
			JOptionPane.showMessageDialog (null, "Search Unsuccessful - book not found", "Search", JOptionPane.INFORMATION_MESSAGE);
			new RequestOtherLib();
		}

		}
		
		
		if (target == requestButton)
		{
			try{
				String it = isbnText.getText();
				String request = "UPDATE Newlibrary set quantity = quantity- 1 WHERE ISBN = "+ it + ";";
				stmt.executeUpdate(request);
				rs = stmt.executeQuery("select * from Newlibrary where ISBN = "+it+"");
				rs.next();
				rs.close();
				JOptionPane.showMessageDialog (null, "Request successfully!", "Search", JOptionPane.INFORMATION_MESSAGE);
			}catch(Exception e2){
				JOptionPane.showMessageDialog (null, "Search Unsuccessful - book not found", "Search", JOptionPane.INFORMATION_MESSAGE);
				new RequestOtherLib();
			}
		}
		
	}
	
	public static void main(String[] args) {
		JFrame Request = new JFrame();
		//new RequestOtherLib();
	}
}
