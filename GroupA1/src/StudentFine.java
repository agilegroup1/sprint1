
	import java.awt.EventQueue;

	import javax.swing.JFrame;
	import java.awt.TextArea;
	import javax.swing.JButton;
	import javax.swing.JTextField;
	import java.awt.event.ActionListener;
	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.Statement;
	import java.awt.event.ActionEvent;
	import javax.swing.JLabel;
	import java.awt.Font;

	public class StudentFine
	{

		private JFrame frame;
		private JTextField finetf;
		private JTextField returndtf;

		// DB Connectivity Attributes
					private Connection con = null;
					private Statement stmt = null;
					private ResultSet rs = null;
					private JTextField daytf;
		/**
		 * Launch the application.
		 */
		public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						StudentFine window = new StudentFine();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		public void initiate_conn() {
			try {
				// Load the JConnector Driver
				Class.forName("com.mysql.jdbc.Driver");
				// Specify the DB Name
				String url = "jdbc:mysql://localhost:3306/Agile_Sprint1";
				// Connect to DB using DB URL, Username and password
				con = DriverManager.getConnection(url, "root", "Ron2393?");
				// Create a generic statement which is passed to the
				// TestInternalFrame1
				stmt = con.createStatement();
				System.out.println("Connected");
			} catch (Exception e) {
				System.out.println("Error: Failed to connect to database\n"
						+ e.getMessage());
			}
		}
		
		/**
		 * Create the application.
		 */
		public StudentFine() {
			initialize();
			initiate_conn();
		}

		/**
		 * Initialize the contents of the frame.
		 */	
		private void initialize() {
			frame = new JFrame();
			frame.setBounds(100, 100, 234, 217);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			

			
			
			finetf = new JTextField();
			finetf.setBounds(118, 128, 98, 20);
			frame.getContentPane().add(finetf);
			finetf.setColumns(10);
			
			JLabel lblNewLabel_3 = new JLabel("Returned Date:");
			lblNewLabel_3.setBounds(10, 78, 67, 14);
			frame.getContentPane().add(lblNewLabel_3);
			
			returndtf = new JTextField();
			returndtf.setBounds(118, 75, 98, 20);
			frame.getContentPane().add(returndtf);
			returndtf.setColumns(10);
			
			JLabel lblCalculateFine = new JLabel(" Calculator");
			lblCalculateFine.setFont(new Font("Vivaldi", Font.PLAIN, 22));
			lblCalculateFine.setBounds(66, 11, 91, 56);
			frame.getContentPane().add(lblCalculateFine);
			
			JButton calculate = new JButton("Calculate \r\nFine");
			calculate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try{
						//String
						int fine=0;
						String cmd = "SELECT DATEDIFF(CURDATE(),'"+returndtf.getText()+"') AS DAY;";
						rs = stmt.executeQuery(cmd);
						System.out.println(cmd);	
						while(rs.next()){
							String day=rs.getString(1);
							daytf.setText(day);
						}
								String day1=daytf.getText();
								int days=Integer.parseInt(day1);
								if(days>0){
									fine=5;
									finetf.setText(""+fine);
								}
								
						
					}catch(Exception e1){
						e1.printStackTrace();
						}
					
					
				}
			});

			calculate.setBounds(10, 128, 108, 21);
			frame.getContentPane().add(calculate);
			
			JLabel lblOverdue = new JLabel("overdue");
			lblOverdue.setBounds(10, 103, 46, 14);
			frame.getContentPane().add(lblOverdue);
			
			daytf = new JTextField();
			daytf.setBounds(118, 100, 98, 20);
			frame.getContentPane().add(daytf);
			daytf.setColumns(10);
		}
	}
