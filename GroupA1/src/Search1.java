import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Search1 extends JFrame implements ActionListener{

	
	// DB Connectivity Attributes
		private Connection con = null;
		private Statement stmt = null;
		private ResultSet rs = null;

	
		JLabel titleLabel = new JLabel ("College Library Search Journal");
		JLabel viewLabel = new JLabel ("Enter Journal IBSN No to search for: ");
		
		JLabel resultLabel = new JLabel ("Result:");
		JLabel risbnLabel = new JLabel ("ISBN:");
		JLabel rtitleLabel = new JLabel ("Title:");
		JLabel rtypeLabel = new JLabel ("Type:");
		JLabel authorLabel = new JLabel ("Author:");
		JLabel quantityLabel = new JLabel ("Quantity:");
		JLabel editionLabel = new JLabel ("Edition:");
		JLabel priceLabel = new JLabel ("Price:");
		JLabel pnameLabel = new JLabel ("Pulisher Name:");
		JLabel paddressLabel = new JLabel ("Publisher Address:");
		JLabel pcontactLabel = new JLabel ("Publisher Contact:");
		JTextField isbnText = new JTextField();
		JTextField risbnText = new JTextField();
		JTextField titleText = new JTextField();
		JTextField typeText = new JTextField();
		JTextField authorText = new JTextField();
		JTextField quantityText = new JTextField();
		JTextField editionText = new JTextField();
		JTextField priceText = new JTextField();
		JTextField pnameText = new JTextField();
		JTextField paddressText = new JTextField();
		JTextField pcontactText = new JTextField();
		JButton searchButton = new JButton("Search for ISBN No");
		JButton menuButton = new JButton("Return to menu");
		
		JPanel panel = new JPanel();
		
		int current = 0;

		
		Search1(){
			initiate_conn();
			setSize(500,700);
			setLocation(10,10);
			panel.setLayout (null); 

			
			titleLabel.setBounds(120,0,250,20);
			viewLabel.setBounds(10,30,250,20);
			resultLabel.setBounds(30,130,50,20);
			risbnLabel.setBounds(30,170,50,20);
			rtitleLabel.setBounds(30,210,50,20);
			rtypeLabel.setBounds(30,250,50,20);
			authorLabel.setBounds(30,290,50,20);
			quantityLabel.setBounds(30,330,50,20);
			editionLabel.setBounds(30,370,50,20);
			priceLabel.setBounds(30,410,50,20);
			pnameLabel.setBounds(30,450,100,20);
			paddressLabel.setBounds(30,490,120,20);
			pcontactLabel.setBounds(30,530,120,20);
			
			isbnText.setBounds(270,30,130,30);
			risbnText.setBounds(150,160,190,30);
			titleText.setBounds(150,200,190,30);
			typeText.setBounds(150,240,190,30);
			authorText.setBounds(150,280,190,30);
			quantityText.setBounds(150,320,190,30);
			editionText.setBounds(150,360,190,30);
			priceText.setBounds(150,400,190,30);
			pnameText.setBounds(150,440,190,30);
			paddressText.setBounds(150,480,190,30);
			pcontactText.setBounds(150,520,190,30);
			
			searchButton.setBounds(190,70,160,30);
			menuButton.setBounds(190,600,160,30);
			
		
			panel.add(risbnLabel);
			panel.add(titleLabel);
			panel.add(viewLabel);
			panel.add(resultLabel);
			panel.add(risbnLabel);
			panel.add(rtitleLabel);
			panel.add(rtypeLabel);
			panel.add(authorLabel);
			panel.add(quantityLabel);
			panel.add(editionLabel);
			panel.add(priceLabel);
			panel.add(pnameLabel);
			panel.add(paddressLabel);
			panel.add(pcontactLabel);
			
			panel.add(isbnText);
			panel.add(risbnText);
			panel.add(titleText);
			panel.add(typeText);
			panel.add(authorText);
			panel.add(quantityText);
			panel.add(editionText);
			panel.add(priceText);
			panel.add(pnameText);
			panel.add(paddressText);
			panel.add(pcontactText);
			panel.add(searchButton);
			panel.add(menuButton);
			
			
		
			searchButton.addActionListener(this);
			menuButton.addActionListener(this);

			getContentPane().add(panel);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setVisible(true);
		}
		
		public void initiate_conn() {
			try {
				// Load the JConnector Driver
				Class.forName("com.mysql.jdbc.Driver");
				// Specify the DB Name
				String url = "jdbc:mysql://localhost:3306/Agile_Sprint1";
				// Connect to DB using DB URL, Username and password
				con = DriverManager.getConnection(url, "root", "Ron2393?");
				// Create a generic statement which is passed to the
				// TestInternalFrame1
				stmt = con.createStatement();
				System.out.println("Connected");
			} catch (Exception e) {
				System.out.println("Error: Failed to connect to database\n"
						+ e.getMessage());
			}
		}

		//event handling 
		public void actionPerformed(ActionEvent e)
		{
			Object target=e.getSource();
			
			if (target == menuButton)
			{
				
					dispose();
					JFrame AdminMenu = new JFrame();
					new AdminMenu();
					
			}
			
			if (target == searchButton)
			{
				
					
					String it = isbnText.getText();
					//System.out.println("Searching" + it);
					String cmd = "SELECT id, ISBN, title, j_type, author, quantity,"
							+ " edition, price, publisherName, publisherAddress, publisherContact FROM journal_details WHERE ISBN = "
							+ it + ";";
					//it = isbnText.getText();
					System.out.println("Searching" + it);
					try
					{
					PreparedStatement stmt = con.prepareStatement(cmd);
					ResultSet rs = stmt.executeQuery(cmd);
					System.out.println(rs);
					while (rs.next()) {
						String is = rs.getString(2);
						risbnText.setText(is);
						System.out.println("Searching" + is);
						String bt = rs.getString(3);
						titleText.setText(bt);					
						String ty = rs.getString(4);
						typeText.setText(ty);
						String au = rs.getString(5);
						authorText.setText(au);
						String qu = rs.getString(6);
						quantityText.setText(qu);
						String ed = rs.getString(7);
						editionText.setText(ed);
						String pr = rs.getString(8);
						priceText.setText(pr);
						String pn = rs.getString(9);
						pnameText.setText(pn);
						String pa = rs.getString(10);
						paddressText.setText(pa);
						String pc = rs.getString(11);
						pcontactText.setText(pc);
					}
					System.out.println(rs);
				}catch(Exception e1){
					JOptionPane.showMessageDialog (null, "Search Unsuccessful - journal not found", "Search", JOptionPane.INFORMATION_MESSAGE);
					new Search();;}

				}
			//current = Integer.parseInt(isbnText.getText());	
			}

			
		

		public static void main(String[] args) {
			/*JFrame Menu = new JFrame();*/
			new Search1();
		}

	}
