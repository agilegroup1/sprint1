import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;

@SuppressWarnings("serial")
public class JournalsWindowContent extends JInternalFrame implements ActionListener
{	
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private JPanel detailsPanel;
	private JPanel exportButtonPanel;
	//private JPanel exportConceptDataPanel;
	private JScrollPane dbContentsPanel;

	private Border lineBorder;

	private JLabel IDLabel=new JLabel("ID no:                 ");
	private JLabel J_ISBNLabel=new JLabel("ISBN no:                 ");
	private JLabel J_Title_Label=new JLabel("Journals Title:               ");
	private JLabel J_Type_Label=new JLabel("Journal type:      ");
	private JLabel J_Author_Label=new JLabel("Author:        ");
	private JLabel J_Quantity_Label=new JLabel("Quantity:                 ");
	private JLabel J_Edition_Label=new JLabel("Edition:               ");
	private JLabel J_Price_Label=new JLabel("Price:      ");
	private JLabel J_publisherN_Label=new JLabel("Publisher Name:      ");
	
	private JLabel J_publisherA_Label=new JLabel("Publisher Address:      ");
	private JLabel J_publisherC_Label=new JLabel("Publisher contact:        ");
	
	//mising apub add

	private JTextField IDTF= new JTextField(10);
	private JTextField J_ISBN_TF= new JTextField(10);
	private JTextField J_Title_TF=new JTextField(10);
	private JTextField J_Type_TF=new JTextField(10);
	private JTextField J_Author_TF=new JTextField(10);
	private JTextField J_Quantity_TF=new JTextField(10);
	private JTextField J_Edition_TF=new JTextField(10);
	private JTextField J_Price_TF=new JTextField(10);
	private JTextField J_publisherN_TF=new JTextField(10);
	private JTextField J_publisherA_TF=new JTextField(10);
	private JTextField J_publisherC_TF=new JTextField(10);
	
	

	private static QueryTableModel2 TableModel = new QueryTableModel2();
	//Add the models to JTabels
	private JTable TableofDBContents=new JTable(TableModel);
	//Buttons for inserting, and updating members
	//also a clear button to clear details panel
	private JButton updateButton = new JButton("Update");
	private JButton addButton = new JButton("Add");
	private JButton viewButton  = new JButton("View");
	private JButton deleteButton  = new JButton("Delete");
	private JButton clearButton  = new JButton("Clear");
	private JButton nextButton = new JButton("Next");
	private JButton prevButton = new JButton("Prev");
 
	int current = 0;

	public JournalsWindowContent( String aTitle)
	{	
		//setting up the GUI
		super(aTitle, false,false,false,false);
		setEnabled(true);

		initiate_db_conn();
		//add the 'main' panel to the Internal Frame
		content=getContentPane();
		content.setLayout(null);
		content.setBackground(Color.DARK_GRAY);
		lineBorder = BorderFactory.createEtchedBorder(15, Color.red, Color.black);

		//setup details panel and add the components to it
		detailsPanel=new JPanel();
		detailsPanel.setLayout(new GridLayout(11,2));
		detailsPanel.setBackground(Color.white);
		detailsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "CRUD Actions"));

		detailsPanel.add(IDLabel);			
		detailsPanel.add(IDTF);
		detailsPanel.add(J_ISBNLabel);
		detailsPanel.add(J_ISBN_TF);
		detailsPanel.add(J_Title_Label);		
		detailsPanel.add(J_Title_TF);
		detailsPanel.add(J_Type_Label);		
		detailsPanel.add(J_Type_TF);
		detailsPanel.add(J_Author_Label);	
		detailsPanel.add(J_Author_TF);
		detailsPanel.add(J_Quantity_Label);		
		detailsPanel.add(J_Quantity_TF);
		detailsPanel.add(J_Edition_Label);
		detailsPanel.add(J_Edition_TF);
		detailsPanel.add(J_Price_Label);
		detailsPanel.add(J_Price_TF);
		detailsPanel.add(J_publisherN_Label);
		detailsPanel.add(J_publisherN_TF);
		detailsPanel.add(J_publisherA_Label);
		detailsPanel.add(J_publisherA_TF);
		detailsPanel.add(J_publisherC_Label);
		detailsPanel.add(J_publisherC_TF);
		

		 
		addButton.setSize(100, 20);
		updateButton.setSize(100, 20);
		viewButton.setSize (100, 20);
		deleteButton.setSize (100, 20);
		clearButton.setSize (100, 20);
		nextButton.setSize(100,20);
		prevButton.setSize(100,20);

		addButton.setLocation(370, 10);
		updateButton.setLocation(370, 90);
		viewButton.setLocation (370, 130);
		deleteButton.setLocation (370, 50);
		clearButton.setLocation (370, 170);
		nextButton.setLocation(370, 210);
		prevButton.setLocation(370, 250);

		addButton.addActionListener(this);
		updateButton.addActionListener(this);
		viewButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);
		nextButton.addActionListener(this);
		prevButton.addActionListener(this);

		

		content.add(addButton);
		content.add(updateButton);
		content.add(viewButton);
		content.add(deleteButton);
		content.add(clearButton);
		content.add(nextButton);
		content.add(prevButton);
		
		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel=new JScrollPane(TableofDBContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(Color.lightGray);
		dbContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder,"Database Content"));

		detailsPanel.setSize(360, 300);
		detailsPanel.setLocation(3,0);
		dbContentsPanel.setSize(700, 300);
		dbContentsPanel.setLocation(477, 0);

		content.add(detailsPanel);
		content.add(dbContentsPanel);

		setSize(982,645);
		setVisible(true);

		TableModel.refreshFromDB(stmt);
	}

	public void initiate_db_conn()
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/Agile_Sprint1";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "Ron2393?");
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}

	//event handling 
	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		if (target == clearButton)
		{
			IDTF.setText("");
			J_ISBN_TF.setText("");
			J_Title_TF.setText("");
			J_Type_TF.setText("");
			J_Author_TF.setText("");
			J_Quantity_TF.setText("");
			J_Edition_TF.setText("");
			J_Price_TF.setText("");
			J_publisherN_TF.setText("");
			J_publisherA_TF.setText("");
			J_publisherC_TF.setText("");
			
		}

		if (target == addButton)
		{		 
			try
			{
				
				String updateTemp ="INSERT INTO journal_details VALUES("+
				null +",'"+J_ISBN_TF.getText()+"','"+J_Title_TF.getText()+"','"+J_Type_TF.getText()+"','"+J_Author_TF.getText()+"',"+J_Quantity_TF.getText()+",'"
				+J_Edition_TF.getText()+"','"+J_Price_TF.getText()+"','"+J_publisherN_TF.getText()+"','"+J_publisherA_TF.getText()+"',"+J_publisherC_TF.getText()+");";

				stmt.executeUpdate(updateTemp);

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  insert:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		
		
		if (target == deleteButton)
		{

			try
			{
				String updateTemp ="DELETE FROM journal_details WHERE id = "+IDTF.getText()+";"; 
				stmt.executeUpdate(updateTemp);

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with delete:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		
		if (target == nextButton)
		{
			current++;
			try
			{
				String updateTemp ="SELECT * FROM journal_details WHERE id = "+ current+";";
				PreparedStatement stmt = con.prepareStatement(updateTemp);
				ResultSet rs = stmt.executeQuery(updateTemp);
				if (rs.next()) {
					String id = rs.getString(1);
					IDTF.setText(id);
					String jisbn = rs.getString(2);
					J_ISBN_TF.setText(jisbn);
					String jtitle = rs.getString(3);
					J_Title_TF.setText(jtitle);
					String jtype = rs.getString(4);
					J_Type_TF.setText(jtype);
					String jauthor = rs.getString(5);
					J_Author_TF.setText(jauthor);
					String Jquantity = rs.getString(6);
					J_Quantity_TF.setText(Jquantity);
					String jedition = rs.getString(7);
					J_Edition_TF.setText(jedition);
					String jprice = rs.getString(8);
					J_Price_TF.setText(jprice);
					String jpubname = rs.getString(9);
					J_publisherN_TF.setText(jpubname);
					String jpubadd = rs.getString(10);
					J_publisherA_TF.setText(jpubadd);
					String jpubcon = rs.getString(11);
					J_publisherC_TF.setText(jpubcon);
				}
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with next \n");
			}
		}
		
		if (target == prevButton)
		{
			current--;
			try
			{
				String updateTemp ="SELECT * FROM journal_details WHERE id = " + current+";";
				PreparedStatement stmt = con.prepareStatement(updateTemp);
				ResultSet rs = stmt.executeQuery(updateTemp);
				if(rs.next()) {
					String id = rs.getString(1);
					IDTF.setText(id);
					String jisbn = rs.getString(2);
					J_ISBN_TF.setText(jisbn);
					String jtitle = rs.getString(3);
					J_Title_TF.setText(jtitle);
					String jtype = rs.getString(4);
					J_Type_TF.setText(jtype);
					String jauthor = rs.getString(5);
					J_Author_TF.setText(jauthor);
					String Jquantity = rs.getString(6);
					J_Quantity_TF.setText(Jquantity);
					String jedition = rs.getString(7);
					J_Edition_TF.setText(jedition);
					String jprice = rs.getString(8);
					J_Price_TF.setText(jprice);
					String jpubname = rs.getString(9);
					J_publisherN_TF.setText(jpubname);
					String jpubadd = rs.getString(10);
					J_publisherA_TF.setText(jpubadd);
					String jpubcon = rs.getString(11);
					J_publisherC_TF.setText(jpubcon);
				}
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with previous \n");
			}
		}
		
		if (target == viewButton)
		{
				String updateTemp ="SELECT id, ISBN, title, j_type, author, quantity, edition, price, publisherName, publisherAddress, publisherContact FROM journal_details WHERE id = "+IDTF.getText()+";"; 
				
				try
				{
					PreparedStatement stmt = con.prepareStatement(updateTemp);
					ResultSet rs = stmt.executeQuery(updateTemp);
					while(rs.next()) {
						String jisbn = rs.getString(2);
						J_ISBN_TF.setText(jisbn);
						String jtitle = rs.getString(3);
						J_Title_TF.setText(jtitle);
						String jtype = rs.getString(4);
						J_Type_TF.setText(jtype);
						String jauthor = rs.getString(5);
						J_Author_TF.setText(jauthor);
						String Jquantity = rs.getString(6);
						J_Quantity_TF.setText(Jquantity);
						String jedition = rs.getString(7);
						J_Edition_TF.setText(jedition);
						String jprice = rs.getString(8);
						J_Price_TF.setText(jprice);
						String jpubname = rs.getString(9);
						J_publisherN_TF.setText(jpubname);
						String jpubadd = rs.getString(10);
						J_publisherA_TF.setText(jpubadd);
						String jpubcon = rs.getString(11);
						J_publisherC_TF.setText(jpubcon);
					}
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with delete:\n"+sqle.toString());
			}
				current = Integer.parseInt(IDTF.getText());
	}
		
		if (target == updateButton)
		{	 	
			try
			{ 			
				String updateTemp ="UPDATE journal_details SET " +
				"ISBN = '"+J_ISBN_TF.getText()+	
				"', title = '"+J_Title_TF.getText()+
				"', j_type = '"+J_Type_TF.getText()+
				"', author = '"+J_Author_TF.getText()+
				"', quantity ="+J_Quantity_TF.getText()+
				", edition = '"+J_Edition_TF.getText()+
				"', price = '"+J_Price_TF.getText()+
				"', publisherName = '"+J_publisherN_TF.getText()+
				"', publisherAddress = '"+J_publisherA_TF.getText()+
				"', publisherContact = "+J_publisherC_TF.getText()+
				" where id = "+IDTF.getText()+";";


				stmt.executeUpdate(updateTemp);
				//these lines do nothing but the table updates when we access the db.
				rs = stmt.executeQuery("SELECT * from journal_details ");
				rs.next();
				rs.close();	
			}
			catch (SQLException sqle){
				System.err.println("Error with  update:\n"+sqle.toString());
			}
			finally{
				TableModel.refreshFromDB(stmt);
			}
		}
	}
		/////////////////////////////////////////////////////////////////////////////////////
		//I have only added functionality of 2 of the button on the lower right of the template
		///////////////////////////////////////////////////////////////////////////////////


	/*private void writeToFile(ResultSet rs){
		try{
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("Ahmed.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();}
	}*/
}
