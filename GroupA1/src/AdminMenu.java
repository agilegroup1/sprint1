import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AdminMenu extends JFrame implements ActionListener{
	
	
		JButton addBooksButton = new JButton("Add Books");
		JButton updateBooksButton = new JButton("Update Books");
		JButton addJournalsButton = new JButton("Add Journals");
		JButton updateJournalsButton = new JButton("Update Journals");
		JButton searchBButton = new JButton("Search Books");
		JButton searchJButton = new JButton("Search Journals");
		JButton viewButton = new JButton("View Requests");
		JButton notificationsButton = new JButton("Notifications");
		JButton calculateButton = new JButton("Fines");
		JButton requestButton = new JButton("Request Lib_No.2");
		JButton exitButton = new JButton("Exit");
		JPanel panel = new JPanel(); 
		JLabel titleLabel = new JLabel ("College Library Admin Menu");
	
	

	AdminMenu(){
		super("Admin Menu");
		setSize(400,300);
		setLocation(500,280);
		panel.setLayout (null); 

		titleLabel.setBounds(120,0,200,20);
		addBooksButton.setBounds(20,30, 160, 20);
		addJournalsButton.setBounds(200,30,160,20);
		searchBButton.setBounds(20,60, 160, 20);
		searchJButton.setBounds(200,60, 160, 20);
		viewButton.setBounds(20,90,160,20);
		notificationsButton.setBounds(200,90,160,20);
		calculateButton.setBounds(20,120,160,20);
		requestButton.setBounds(200,120,160,20);
		exitButton.setBounds(20,150,160,20);
		

		panel.add(titleLabel);
		panel.add(addBooksButton);
		panel.add(addJournalsButton);
		panel.add(searchBButton);
		panel.add(searchJButton);
		panel.add(viewButton);
		panel.add(notificationsButton);
		panel.add(calculateButton);
		panel.add(requestButton);
		panel.add(exitButton);
		
		addBooksButton.addActionListener(this);
		addJournalsButton.addActionListener(this);
		searchBButton.addActionListener(this);
		searchJButton.addActionListener(this);
		viewButton.addActionListener(this);
		notificationsButton.addActionListener(this);
		calculateButton.addActionListener(this);
		requestButton.addActionListener(this);
		exitButton.addActionListener(this);

		getContentPane().add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		if (target == addBooksButton)
			{
			JFrame BooksWindow = new JFrame();
			new BooksWindow();}
		
		if (target == addJournalsButton)
		{
			JFrame JournalsWindow = new JFrame();
			new JournalsWindow();
			}
		
		if (target == searchBButton)
		{
			JFrame Search = new JFrame();
			new Search();
		}
		
		if (target == searchJButton)
		{
			JFrame Search1 = new JFrame();
			new Search1();
		}
		
		/*
		if (target == viewButton)
		{
			JFrame View = new JFrame();
			new View();
		}
		*/
		if (target == viewButton)
		{
			JFrame ViewRequest = new JFrame();
			 new MainViewRequests();
			 
		}
		
		if (target == notificationsButton)
		{
			JFrame Notifications = new JFrame();
			new Notifications();
		}
		
		if (target == calculateButton)
		{
			JFrame Calculate = new JFrame();
			new Calculate();
		}
		
		if(target == requestButton)
		{
			JFrame Request = new JFrame();
			new RequestOtherLib();
		}
		
		if (target == exitButton)
		{
			dispose();
			JFrame LoginGui = new JFrame();
			new LoginGui();
			
		}
		
	}	

	
	public static void main(String[] args) {
		JFrame AdminMenu = new JFrame();
		}

	}
