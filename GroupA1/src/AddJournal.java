import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AddJournal extends JFrame implements ActionListener{

	
	// DB Connectivity Attributes
			private Connection con = null;
			private Statement stmt = null;
			private ResultSet rs = null;

		
			JLabel titleLabel = new JLabel ("College Library Add/Update Journal");
			JButton addButton = new JButton("Add journal");
			JLabel ISBNLabel = new JLabel ("ISBN No");
			JTextField isbn = new JTextField(12);
			JLabel journalTitleLabel = new JLabel ("Journal Title");
			JTextField journalTitle = new JTextField(30);
			JLabel journalTypeLabel = new JLabel ("Journal Type");
			JTextField journaltype = new JTextField(30);
			JLabel authorLabel = new JLabel ("Author");
			JTextField author = new JTextField(30);
			JLabel quantityLabel = new JLabel ("Quantity");
			JTextField quantity = new JTextField(30);
			JLabel editionLabel = new JLabel ("Edition");
			JTextField edition = new JTextField(12);
			JLabel priceLabel = new JLabel ("Price");
			JTextField price = new JTextField(9);
			JLabel pubNameLabel = new JLabel ("Publisher Name");
			JTextField pubName = new JTextField(30);
			JLabel pubEmailLabel = new JLabel ("Publisher Email");
			JTextField pubEmail = new JTextField(30);
			JLabel pubPhoneLabel = new JLabel ("Publisher Phone No");
			JTextField pubPhoneNo = new JTextField(30);
			
			JPanel panel = new JPanel();

			AddJournal(){
			super("College Library");
			setSize(500,800);
			setLocation(400,280);
			panel.setLayout (null); 

			titleLabel.setBounds(120,0,200,20);
			
			ISBNLabel.setBounds(50,30,100,20);
			isbn.setBounds(190,30,100,20);
			journalTitleLabel.setBounds(50,50,100,20);
			journalTitle.setBounds(190,50,100,20);
			journalTypeLabel.setBounds(50,70,100,20);
			journaltype.setBounds(190,70,100,20);
			authorLabel.setBounds(50,90,100,20);
			author.setBounds(190,90,100,20);
			quantityLabel.setBounds(50,110,100,20);
			quantity.setBounds(190,110,100,20);
			editionLabel.setBounds(50,130,100,20);
			edition.setBounds(190,130,100,20);
			priceLabel.setBounds(50,150,100,20);
			price.setBounds(190,150,100,20);
			pubNameLabel.setBounds(50,190,100,20);
			pubName.setBounds(190,170,100,20);
			pubEmailLabel.setBounds(50,170,100,20);
			pubEmail.setBounds(190,190,100,20);
			pubPhoneLabel.setBounds(50,210,120,20);
			pubPhoneNo.setBounds(190,210,100,20);
			
			addButton.setBounds(190,250,100,30);
				
			panel.add(titleLabel);
			panel.add(ISBNLabel);
			panel.add(isbn);
			panel.add(journalTitleLabel);
			panel.add(journalTitle);
			panel.add(journalTypeLabel);
			panel.add(journaltype);
			panel.add(authorLabel);
			panel.add(author);
			panel.add(quantityLabel);
			panel.add(quantity);
			panel.add(editionLabel);
			panel.add(edition);
			panel.add(priceLabel);
			panel.add(price);
			panel.add(pubNameLabel);
			panel.add(pubName);
			panel.add(pubEmailLabel);
			panel.add(pubEmail);
			panel.add(pubPhoneLabel);
			panel.add(pubPhoneNo);		
			
			panel.add(addButton);
		
			addButton.addActionListener(this);

			getContentPane().add(panel);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setVisible(true);
		}
		
		//event handling 
			public void actionPerformed(ActionEvent e)
			{
				Object target=e.getSource();
				if (target == addButton)
				{
					/*try
					{
						
						String Temp ="INSERT INTO students VALUES("+
								null +",'"+journaltype.getText()+"','"+edition.getText()+"',"+ISBNLabel.getText()+"');";

						stmt.executeUpdate(Temp);
						
						String updateTemp ="INSERT INTO student_login VALUES("+null +",'"+journaltype.getText()+"');";

						stmt.executeUpdate(updateTemp);

					}
					catch (SQLException sqle)
					{
						System.err.println("Error with  insert:\n"+sqle.toString());
					}
					JOptionPane.showMessageDialog (null, "Student Reg Successful", "Student eg", JOptionPane.INFORMATION_MESSAGE);
						JFrame LoginGui = new JFrame();
						new LoginGui();
						dispose();}	*/
					new AdminMenu();
					dispose();
					}
				}
		
		public static void main(String[] args) {
			JFrame Menu = new JFrame();
			}

		}