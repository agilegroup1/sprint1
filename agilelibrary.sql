CREATE DATABASE IF NOT EXISTS Agile_Sprint1;
USE Agile_Sprint1;

/*/////////////////////////////////////////////////////////////////////////*/

DROP TABLE IF EXISTS admin_login;


CREATE TABLE admin_login(
	uname VARCHAR(6) NOT NULL PRIMARY KEY,
	pword VARCHAR(15) NOT NULL);

SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO admin_login VALUES ('admin1', '1234' );
INSERT INTO admin_login VALUES ('admin2', '4321' );

select * from admin_login;

/*/////////////////////////////////////////////////////////////////////////*/

DROP TABLE IF EXISTS student_login;


CREATE TABLE student_login(
	uname VARCHAR(9) NOT NULL PRIMARY KEY,
	pword VARCHAR(15) NOT NULL);

SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO student_login VALUES ('A00212975', 'paul1966' );

select * from student_login;

/*/////////////////////////////////////////////////////////////////////////*/

DROP TABLE IF EXISTS students;


CREATE TABLE students(
	student_no VARCHAR(9) NOT NULL PRIMARY KEY,
	student_name VARCHAR(30) NOT NULL,
	student_email VARCHAR(30) NOT NULL,
	pword VARCHAR(30) NOT NULL);

SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO students VALUES ('A00212975', 'Paul Cordial', 'A00212975@student.ait.ie', 'paul1966' );

select * from students;

/*/////////////////////////////////////////////////////////////////////////*/

DROP TABLE IF EXISTS addbooks;

CREATE TABLE addbooks(
	ISBN VARCHAR(9) NOT NULL PRIMARY KEY,
	bookTitle VARCHAR(30) NOT NULL,
	bookType VARCHAR(30) NOT NULL,
    author VARCHAR(30) NOT NULL,
	quantity INTEGER(3) NOT NULL,
	edition INTEGER(3) NOT NULL,
	price FLOAT(9,2) NOT NULL,
	publisherName VARCHAR(30) NOT NULL,
	pubisherEmail VARCHAR(30) NOT NULL,
	publisherPhone VARCHAR(30) NOT NULL);
	
	SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO addbooks VALUES ('IT0000101', 'Java for Dummies', 'Technology', 'D.Byrne', 8, 3, 24.99, 'Penguin', 'penguin@gmail.com', '091 125 3656' );

select * from addbooks;

/*/////////////////////////////////////////////////////////////////////////*/

DROP TABLE IF EXISTS addjournals;

CREATE TABLE addjournals(
	ISBN VARCHAR(9) NOT NULL PRIMARY KEY,
	bookTitle VARCHAR(30) NOT NULL,
	bookType VARCHAR(30) NOT NULL,
    author VARCHAR(30) NOT NULL,
	quantity INTEGER(3) NOT NULL,
	edition INTEGER(3) NOT NULL,
	price FLOAT(9,2) NOT NULL,
	publisherName VARCHAR(30) NOT NULL,
	pubisherEmail VARCHAR(30) NOT NULL,
	publisherPhone VARCHAR(30) NOT NULL);
	
	SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO addjournals VALUES ('IT123J', 'Java Research ', 'Technology', 'J.Byrne', 2, 1, 9.99, 'JavaCity', 'javacity@gmail.com', '081 589 5746' );

select * from addjournals;

/*/////////////////////////////////////////////////////////////////////////*/

/*/////////////////////////////////////////////////////////////////////////*/

